package com.example.videomasak;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

public class MainActivity extends AppCompatActivity {
    MediaController media_Controller;
    DisplayMetrics dm;

    VideoView videoViewSaya;
    TextView txtJudul;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // view
        videoViewSaya = (VideoView) findViewById(R.id.videoViewUtama);
        txtJudul = (TextView) findViewById(R.id.txtJudul);

        // inisialisasi
        media_Controller = new MediaController(this);
        dm = new DisplayMetrics();

        // ambil ukuran layar
        this.getWindowManager().getDefaultDisplay().getMetrics(dm);
        int height = dm.heightPixels;
        int width = dm.widthPixels;

        // lebar dan tinggi video view diberi nilai agar menyesuaikan ukuran layout
        videoViewSaya.setMinimumWidth(width);
        videoViewSaya.setMinimumHeight(height);

        // kontroler video
        videoViewSaya.setMediaController(media_Controller);
    }

    private void muatVideo(String judul, String nama_file) {
        // kasih judul
        txtJudul.setText(judul);

        // ambil video
        videoViewSaya.setVideoPath("android.resource://"+getPackageName()+"/raw/"+nama_file);

        // mulai
        videoViewSaya.start();
    }

    public void video1(View view) {
        muatVideo("Cara Memasak Makaroni Scootel Enak", "makaroni");
    }
    public void video2(View view) {
        muatVideo("Cara Memasak Nasi Goreng", "nasgor");
    }
    public void video3(View view) {
        muatVideo("Cara Memasak Sop Buah", "sopbuah");
    }
}